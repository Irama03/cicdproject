package org.example;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

public class Service {
    public static void printMapToFile(Map<String, String> map, String filename) {
        JSONObject obj = Core.transformMapToJSONObject(map);
        Io.writeJSONObjectToJSONFile(obj, filename);
    }

    public static void printCommaDelimitedTextToFile(String str, String filename) {
        JSONArray arr = Core.transformCommaDelimitedTextToJSONArray(str);
        Io.writeJSONArrayToJSONFile(arr, filename);
    }
}
