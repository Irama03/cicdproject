package org.example;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Io {

    public static void writeJSONObjectToJSONFile(JSONObject obj, String fileName) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("generatedFiles/" + fileName + ".json"))) {
            obj.write(writer);
            writer.write("\n");
        } catch (Exception ex) {
            System.err.println("Couldn't write object\n"
                    + ex.getMessage());
        }
    }

    public static void writeJSONArrayToJSONFile(JSONArray arr, String fileName) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("generatedFiles/" + fileName + ".json"))) {
            arr.write(writer);
            writer.write("\n");
        } catch (Exception ex) {
            System.err.println("Couldn't write array\n"
                    + ex.getMessage());
        }
    }

    public static JSONObject readJSONObjectFromJSONFile(String fileName) {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get("generatedFiles/" + fileName+ ".json"))){
            JSONTokener tokener = new JSONTokener(reader);
            return new JSONObject(tokener);
        } catch (Exception ex) {
            System.err.println("Couldn't read object\n"
                    + ex.getMessage());
        }
        return new JSONObject();
    }

    public static JSONArray readJSONArrayFromJSONFile(String fileName) {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get("generatedFiles/" + fileName+ ".json"))){
            JSONTokener tokener = new JSONTokener(reader);
            return new JSONArray(tokener);
        } catch (Exception ex) {
            System.err.println("Couldn't read array\n"
                    + ex.getMessage());
        }
        return new JSONArray();
    }
}
