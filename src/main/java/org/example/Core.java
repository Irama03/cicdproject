package org.example;

import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

public class Core {

    public static JSONObject transformMapToJSONObject(Map<String, String> map) {
        return new JSONObject(map);
    }

    public static JSONArray transformCommaDelimitedTextToJSONArray(String str) {
        return CDL.rowToJSONArray(new JSONTokener(str));
    }
}
