package org.example;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, String> pets = new HashMap<>();
        pets.put("parrot", "Karrudo");
        pets.put("chinchilla", "Shurik");
        pets.put("cat", "Plamka");
        Service.printMapToFile(pets, "pets");

        String countries = "Ukraine, Canada, Italy, USA, France";
        Service.printCommaDelimitedTextToFile(countries, "countries");
    }
}
