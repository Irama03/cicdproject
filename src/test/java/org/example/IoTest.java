package org.example;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IoTest {

    @Test
    public void checkWritingMyInfoToJSON() {
        Map<String, String> myInfo = new HashMap<>();
        myInfo.put("name", "Ira");
        myInfo.put("surname", "Matviienko");
        myInfo.put("age", "19");
        Service.printMapToFile(myInfo, "myInfo");
        JSONObject obj = Io.readJSONObjectFromJSONFile("myInfo");
        assertEquals(3, obj.length());
        assertEquals("Ira", obj.getString("name"));
    }

    @Test
    public void checkWritingCountriesToJSON() {
        String countries = "Ukraine, Canada, Italy";
        Service.printCommaDelimitedTextToFile(countries, "countries");
        JSONArray arr = Io.readJSONArrayFromJSONFile("countries");
        assertEquals(3, arr.length());
        assertEquals("Ukraine", arr.getString(0));
    }

}
