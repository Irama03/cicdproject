package org.example;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServiceTest {

    @Test
    public void checkWritingAndReadingJSONObjectToJSON() {
        JSONObject obj = new JSONObject();
        obj.put("name", "Ira");
        obj.put("surname", "Matviienko");
        Io.writeJSONObjectToJSONFile(obj, "myInfo");
        obj = Io.readJSONObjectFromJSONFile("myInfo");
        assertEquals(2, obj.length());
        assertEquals("Ira", obj.getString("name"));
    }

    @Test
    public void checkWritingAndReadingJSONArrayToJSON() {
        JSONArray arr = new JSONArray();
        arr.put("Ukraine");
        arr.put("Italy");
        Io.writeJSONArrayToJSONFile(arr, "countries");
        arr = Io.readJSONArrayFromJSONFile("countries");
        assertEquals(2, arr.length());
        assertEquals("Italy", arr.getString(1));
    }
}
