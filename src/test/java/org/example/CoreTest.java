package org.example;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CoreTest {

    @Test
    public void checkTransformingMapToJSONObject() {
        Map<String, String> myInfo = new HashMap<>();
        myInfo.put("name", "Ira");
        myInfo.put("surname", "Matviienko");
        myInfo.put("age", "19");
        JSONObject obj = Core.transformMapToJSONObject(myInfo);
        assertEquals(3, obj.length());
        assertEquals("Ira", obj.getString("name"));
    }

    @Test
    public void checkTransformingCommaDelimitedTextToJSONArray() {
        String countries = "Ukraine, Canada, Italy";
        JSONArray arr = Core.transformCommaDelimitedTextToJSONArray(countries);
        assertEquals(3, arr.length());
        assertEquals("Ukraine", arr.getString(0));
    }
}
